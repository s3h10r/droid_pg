package de.damaru.alog;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.View;

import java.text.SimpleDateFormat;
import java.util.Date;

public class MainActivity extends Activity {

	public final static String EXTRA_TIMESTAMP = "de.damaru.aLog.TIMESTAMP";
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}
	

	public void onClickEntryNew(View view) {

		// adden von aktuellem Zeitstempel als intent.putExtra
		// http://developer.android.com/training/basics/firstapp/starting-activity.html

		Intent intent = new Intent(this, EntryNewActivity.class);

		Date ts = new Date();
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy/MM/dd HH:mm");
		String ts_formatted = sdf.format(ts);
		
		intent.putExtra(EXTRA_TIMESTAMP, ts_formatted);
		
		startActivity(intent);
	}	

}
