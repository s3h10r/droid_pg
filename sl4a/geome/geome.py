#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
geomy.py - location based service playground (ASE,SL4A)


Q: what means position and what location? aren't these the same things?
A: nope (at least not in this app). our terminology here is:
   position = gps coordinates
   location = a saved position with aditional infos (tags, ...)
"""

import android
import datetime,time,math
import threading

__author__ = "Sven Hessenmüller <sven.hessenmueller@gmail.com>"
__copyright__ = "Copyright 2011, www.querzone.de"
__license__ = "GPL"
__version__ = "0.0.3d"
__lastchange__ = "20110904"


FILE_LOCATIONS="./scripts/geome/locations.csv" # relative path to /mnt/sdcard/sl4a/

curPos, lastPos = None, None    
curPosTS, lastPosTS = None,None # timestamp of retrieval

dLocations = {} # dict of already defined locations 


def getPosition(droid,max_wait=50):
    """
    get current location based on GPS
    on my nexus one this usually takes 10-40s

    returns

        dict['latitude'|'altitude'] 
        False
    """

    # we could also use the getLastKnownLocation routine, but this run the risk of having old information.

    droid.startLocating()
    for i in range(10):
        time.sleep(max_wait / 10)
        try:
            loc = droid.readLocation().result
            if not loc.has_key('gps'):
                print "DEBUG: loc is not what we want to have: ", loc
                raise Exception, "nope. got no gps info yet."
            else:
                break
        except:
            print "waiting for gps info... cycle %i" %i
            pass

    loc = droid.readLocation().result
    droid.stopLocating()

    print loc

    if loc.has_key('gps'):
        #return loc['gps']['latitude'], loc['gps']['longitude']
        return loc['gps']
    elif loc.has_key('network'):
        print "no gps, but network - better than nothing, eh." 
        return loc['network']

    else:
        return False

def updatePosition(droid):
    global curPos, curPosTS, lastPos, lastPosTS

    tsnow = datetime.datetime.now()
    lastPos = curPos
    lastPosTS = curPosTS
    curPos = getPosition(droid)
    curPosTS = tsnow

    if not curPos: return False
    else: return curPos


def distance(pos_1, pos_2):
    """
    distance between 2 gps locations
    using the spherical law of cosines from http://www.movable-type.co.uk/scripts/latlong.html
    """

    R = 6371
    lat1 = math.radians(pos_1['latitude'])
    lat2 = math.radians(pos_2['latitude'])
    lon1 = math.radians(pos_1['longitude'])
    lon2 = math.radians(pos_2['longitude'])
    return math.acos( math.sin(lat1)*math.sin(lat2) + math.cos(lat1)*math.cos(lat2) * math.cos(lon2-lon1)) * R


def saveLocation(latitude = None, longitude = None, tags = [], fname = FILE_LOCATIONS, timestamp = None):
    """
    save pos to locations-file

    returns 

        updated locations dictionary
    """

    f = open(fname, 'a+', 0)

    timestamp = str(timestamp); latitude=str(latitude); longitude = str(longitude)
    radius_km = str(0.2)

    tags = ";".join(tags)

    line = ";".join([timestamp,latitude,longitude,radius_km,tags])

    f.write(line + "\n")
    f.close()

    return readLocation()


def readLocation(fname = FILE_LOCATIONS):
    """
    """
    dLocs = {}

    f = open(fname, 'rU')
    for i,l in enumerate(f):
        dLocs[i] = {}

        l = l.strip()
        timestamp,latitude,longitude = l.split(';')[:3]
        radius_km = l.split(';')[3] # radius_km = threshold for which we can trigger as checked into location
        tags = l.split(';')[4:]

        dLocs[i]['latitude'] = float(latitude)
        dLocs[i]['longitude'] = float(longitude)
        dLocs[i]['radius_km'] = float(radius_km)
        dLocs[i]['ts'] = timestamp # TODO ASAP: convert into datetime object
        dLocs[i]['tags'] = tags
    f.close()

    return dLocs

def getLocationNear(radius_km = 1.0, curPos = {}, dLocs = {}):
    lNearLocID = []

    for i in dLocs.keys():
        dist = distance(dLocs[i], curPos)
        if dist <= radius_km: lNearLocID.append(i)

    return lNearLocID


class bgthread(threading.Thread):
    """
    FIXME / TODO
    a thread for doing background stuff (like checking gps pos every n secs, triggering custom actions)

    ???droid-GU OPs are blocking???
    """

    def __init__(self, intervall):
        self.intervall = intervall
        self.bolStop = False
        threading.Thread.__init__(self)

    def run(self):
        while True and not self.bolStop: # check for current gps locations -> checkin etc.
            print "%s ...thread says yop." % datetime.datetime.now()
            print "and should do things here..."

            if updatePosition(droid): print "--info: bgthread: updatePosition() succeeded" # FIXME: this breaks mainloop dialogs...??!!??
            else: print "--warning: bgthread: updatePosition() failed"                # FIXME

            time.sleep(self.intervall)

        print "bgthread exiting..."


    def stop(self):
        self.bolStop = True
        print "bgthread got order to stop..."



def droid_mainloop(droid):
    global curPos,lastPos,curPosTS,lastPosTS,dLocations

    while True:
        title = 'geome %s' % __version__
        droid.dialogCreateAlert(title)
        droid.dialogSetSingleChoiceItems(['current position', 'current location', 'distance to saved locations', 'what\'s near?', 'set action 4 location', 'bla'])
        droid.dialogSetPositiveButtonText('yep!')
        droid.dialogSetNegativeButtonText('nope / exit')
        droid.dialogShow()

        response = droid.dialogGetResponse().result
        selected = droid.dialogGetSelectedItems().result

        droid.dialogDismiss()

        print "response:" , response, "---"
        print "selected: ", selected, "---"
        selected = selected[0]


        if response['which'] == 'negative': break # pressed exit


        if selected == 0: # get current position and allow to save it as location (show|save)
            droid.dialogCreateSpinnerProgress("GPS", "fetching GPS sensor data...")
            droid.dialogShow()

            updatePosition(droid)

            droid.dialogDismiss()

            if not curPos: 
                droid.dialogCreateAlert("Naargh...", "sorry, got no GPS data :-(") # TODO: droid.getLastKnownLocation()
                droid.dialogSetPositiveButtonText('Continue')
                droid.dialogShow()
                response = droid.dialogGetResponse().result
                droid.dialogDismiss()
            else:
                droid.dialogCreateInput("save pos %f,%f as (tags seperated by spaces)" % (curPos['latitude'],curPos['longitude']))
                droid.dialogSetPositiveButtonText('save')
                droid.dialogSetNeutralButtonText('cancel')
                droid.dialogShow()
                response = droid.dialogGetResponse().result
                tags = response['value'].split()
                droid.dialogDismiss()

                if response['which'] == 'positive': 
                    dLocations = saveLocation(curPos['latitude'], curPos['longitude'], tags, FILE_LOCATIONS, curPosTS) # save pos and update known locations
                    droid.makeToast("TODO: position saved...")
                else:
                    droid.makeToast("aborted...")
        
        elif selected == 1: # show if pos is inside a defined location (inside == predefined radius)
            droid.makeToast("TODO: show if curPos is inside a defined location (radius)")

        elif selected == 2: # distance to saved locations

            lLocs = [ str(id) + str(dLocations[id]['tags']) for id in sorted(dLocations.keys()) ]
            

            droid.dialogCreateAlert("distance to saved locations...")
            droid.dialogSetSingleChoiceItems(lLocs)
            droid.dialogSetPositiveButtonText('yep!')
            droid.dialogSetNegativeButtonText('cancel')
            droid.dialogShow()
            response = droid.dialogGetResponse().result
            selected = droid.dialogGetSelectedItems().result[0]
            droid.dialogDismiss()

            if response['which'] == 'positive':

                droid.dialogCreateSpinnerProgress("GPS", "fetching GPS sensor data...")
                droid.dialogShow()
                if updatePosition(droid):
                    dist = distance(dLocations[selected], curPos)
                    droid.makeToast("distance: %.3f km" % dist)
                else:
                    droid.dialogCreateAlert("Naargh...", "sorry, got no GPS data :-(") 
                    droid.dialogSetPositiveButtonText('Continue')
                    droid.dialogShow()
                    response = droid.dialogGetResponse().result
                    droid.dialogDismiss()

                droid.dialogDismiss()

                
            else:
                droid.makeToast("aborted...")
            
        elif selected == 3: # what's near
            droid.dialogCreateInput("radius in kilometers?")
            droid.dialogSetPositiveButtonText('OK')
            droid.dialogSetNeutralButtonText('Cancel')
            droid.dialogShow()
            response = droid.dialogGetResponse().result
            radius_km = float(response['value'])
            droid.dialogDismiss()

            lNear = getLocationNear(radius_km,curPos,dLocations)

            print lNear

            droid.dialogCreateAlert("locations inside radius of %f km" % radius_km)
            droid.dialogSetSingleChoiceItems(lNear)
            droid.dialogSetPositiveButtonText('ok')
            droid.dialogShow()
            response = droid.dialogGetResponse().result
            selected = droid.dialogGetSelectedItems().result[0]
            droid.dialogDismiss()

        else:
            pass
         
 

dLocations = readLocation()
print "dLocations: %s" % dLocations
print "read %i locations" % len(dLocations.keys())

droid = android.Android()

#thread = bgthread(60*2) # intervall for background operations
#thread.start() # FIXME: nargh. this breaks dialog wtith coices ??? ;-(

droid_mainloop(droid)
droid.makeToast("Bye!")

thread.stop()
