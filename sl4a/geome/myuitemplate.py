#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
myuitemplate.py - a background threads supporting gui build of standard dialoges

TODOs:

- thread order to stop: better solution (faster) -> time-diff checking instead of sleep
- examples for more basic dialog elements
- running more than 1 background threads ...

"""

"""
NOTES2MYSELF:
- eventWait, dialogGet... ARE BLOCKING other thread's droid.-calls, so better poll or use with care
- getSelectedItems returns sometimes crap, if in another thread a gps-event is generated?
"""

import android
import datetime,time,math
import threading

__author__ = "Sven Hessenmüller <sven.hessenmueller@gmail.com>"
__copyright__ = "Copyright 2011, www.querzone.de"
__license__ = "GPL"
__version__ = "0.0.1"
__lastchange__ = "20110907"


class bgthread(threading.Thread):
    """
    """

    def __init__(self, intervall):
        self.intervall = intervall
        self.bolStop = False
        threading.Thread.__init__(self)

    def run(self):
        while True and not self.bolStop: # check for current gps locations -> checkin etc.
            print "%s ...thread says yop." % datetime.datetime.now()
            print "and should do things here..."

            droid.startLocating()

            for i in range(10):
                time.sleep(50 / 10)
                try:
                    loc = droid.readLocation().result # seemst to get blocked by droid.dialogGet...

                    if not loc.has_key('gps'):
                        #print "DEBUG: loc is not what we want to have: ", loc
                        raise Exception, "nope. got no gps info yet."
                    else:
                        break
                except:
                    print "...waiting for gps info... cycle %i" %i
                    pass

            droid.stopLocating()
            time.sleep(self.intervall)

        print "bgthread exiting..."


    def stop(self):
        self.bolStop = True
        print "bgthread got order to stop..."


def dialogEventloop():
    """
    poll events. if they are ui-events return (them).
    this is hopefully a working way to avoid blocking all other droid.-calls (in parallel threads)
    by waiting for just a gui-response.
    """

    now = datetime.datetime.now()

    dR = {}
    dR['dialogGetResponse'], dR['dialogGetSelectedItems'] = None,None

    while True:
        event = droid.eventPoll(1).result
        if event:
            print now, "got event: ", event
            #BUGFIXED, comment the following 2 lines in to show it 
            #print "droid Response", droid.dialogGetResponse() # <- this blocks other droid.calls
            #print "droid SelectedItems", droid.dialogGetSelectedItems() # <- BUG HERE we can get networking results... FUNNY :-(
            # BUGFIXED

            if type(event) == list and len(event) == 1:
                event = event[0]
                if event['name'] == 'dialog':
                    dR['dialogGetResponse'] = droid.dialogGetResponse()
                    dR['dialogGetSelectedItems'] = droid.dialogGetSelectedItems()
                    break
            else:
                print "nargh. event.result has more than one element..."
                for el in event: print el, "\n"
  
        time.sleep(1)
        
    return dR


def droid_mainloop(droid):
    while True:
        title = 'myuitemplate %s' % __version__

        droid.dialogCreateAlert(title, "a (hopefully working :) background threads supporting gui build of standard dialoges.")
        droid.dialogSetPositiveButtonText('yep!')
        droid.dialogSetNegativeButtonText('nope / exit')
        droid.dialogShow()

        dEvents = dialogEventloop()
        droid.dialogDismiss()

        print dEvents['dialogGetResponse']
        print dEvents['dialogGetSelectedItems']

        droid.dialogCreateAlert("main menu", "plz choose your stuff...")
        droid.dialogSetSingleChoiceItems(['show spinner...', 'or choice 1', 'or 2', 'or what?'])
        droid.dialogSetPositiveButtonText('yep!')
        droid.dialogSetNegativeButtonText('nope / exit')
        droid.dialogShow()

        dEvents = dialogEventloop()
        droid.dialogDismiss()

        print dEvents['dialogGetResponse']
        print dEvents['dialogGetSelectedItems']

        response = dEvents['dialogGetResponse'].result
        selected = dEvents['dialogGetSelectedItems'].result[0]

        print response
        print selected

        if response['which'] == 'negative': break # pressed exit


        if selected == 0: 
            droid.dialogCreateSpinnerProgress("heyho...", "...let's just spinning 5 seconds...")
            droid.dialogShow()
            time.sleep(5)
            droid.dialogDismiss()
        elif selected == 1: 
            droid.makeToast("you selected 1")
        else:
            pass


droid = android.Android()

thread = bgthread(60*2) # intervall for background operations
thread.start() # FIXME: nargh. this breaks dialog wtith coices ??? ;-(

droid_mainloop(droid)
droid.makeToast("Bye!")

thread.stop()
